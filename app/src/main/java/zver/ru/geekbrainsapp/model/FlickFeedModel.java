package zver.ru.geekbrainsapp.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class FlickFeedModel {

    @Ignore
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

    @PrimaryKey
    public int id;

    public String name;

    @TypeConverters(RoomDateConverter.class)
    public Date feedDate;
    public String feedDescription;
    public String comment;

    public static class RoomDateConverter{
        @TypeConverter
        public String fromDate(Date date){
            return dateFormat.format(date);
        }

        @TypeConverter
        public Date fromString(String date){
            try {
                return dateFormat.parse(date);
            }catch (ParseException e){
                return new Date();
            }
        }
    }

    public FlickFeedModel(String name, Date feedDate, String feedDescription) {
        this.name = name;
        this.feedDate = feedDate;
        this.feedDescription = feedDescription;
    }

    public String getFeedDateAsString(){
        return dateFormat.format(feedDate);
    }
}
