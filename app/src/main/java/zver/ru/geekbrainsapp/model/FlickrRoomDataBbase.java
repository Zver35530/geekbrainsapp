package zver.ru.geekbrainsapp.model;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomDatabase;

import zver.ru.geekbrainsapp.presentor.RoomFeedModelDao;

@Database(entities = {FlickFeedModel.class}, version = 1, exportSchema = false)
public abstract class FlickrRoomDataBbase extends RoomDatabase {

    public abstract RoomFeedModelDao getDao();

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }
}
