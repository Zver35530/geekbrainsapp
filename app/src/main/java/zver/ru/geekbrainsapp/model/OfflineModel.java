package zver.ru.geekbrainsapp.model;

/***
 * На случай если нет подключения, используем то, что скачано
 */
public class OfflineModel extends AbstractModel {
    public OfflineModel(String name) {
        super(name);
    }
}
