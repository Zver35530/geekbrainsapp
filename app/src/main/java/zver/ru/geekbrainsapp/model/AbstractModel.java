package zver.ru.geekbrainsapp.model;

import com.orm.SugarRecord;

public class AbstractModel extends SugarRecord {

    public final String name;

    public AbstractModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
