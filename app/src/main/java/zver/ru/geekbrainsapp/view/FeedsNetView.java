package zver.ru.geekbrainsapp.view;

import java.util.List;

import zver.ru.geekbrainsapp.model.FlickFeedModel;

public interface FeedsNetView extends BaseNetView {

    void setFeeds(List<FlickFeedModel> feedModels);
}
