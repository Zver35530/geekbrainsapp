package zver.ru.geekbrainsapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import zver.ru.geekbrainsapp.LoginActivity;
import zver.ru.geekbrainsapp.R;
import zver.ru.geekbrainsapp.model.FlickFeedModel;
import zver.ru.geekbrainsapp.presentor.FlickrFeedPresentor;

public class FeedActivity extends AppCompatActivity
        implements FeedsNetView, NavigationView.OnNavigationItemSelectedListener {

    RecyclerView feedsList;
    List<FlickFeedModel> modelList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //todo Проверка на наличие авторизации
        if(false){
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
        }

        feedsList = (RecyclerView) findViewById(R.id.feedsList);
        feedsList.setLayoutManager( new LinearLayoutManager(this));
        FlickrFeedPresentor presentor = new FlickrFeedPresentor(this);
        presentor.subscribe(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.download) {
            // Handle the camera action
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDownLoad() {

    }

    @Override
    public void onDownloadComplete() {

    }

    @Override
    public void showEmpty() {
        Toast.makeText(this, "Пичалька, ничего нет", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setFeeds(List<FlickFeedModel> feedModels) {
        feedsList.setAdapter(new FlickrFeedArrayAdtp(feedModels));
        return;
    }

    @Override
    public void onSubscribe(Subscription s) {
        s.request(Long.MAX_VALUE);
    }

    @Override
    public void onNext(Object o) {
        modelList.add((FlickFeedModel) o);
    }

    @Override
    public void onError(Throwable t) {
        Toast.makeText(this, "При загрузке возникли ошибки", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {
        feedsList.setAdapter(new FlickrFeedArrayAdtp(modelList));
        Toast.makeText(this, "Данные загружены", Toast.LENGTH_SHORT).show();
    }

    private class FlickrFeedArrayAdtp extends RecyclerView.Adapter<FlickrFeedArrayAdtp.FeedHolder>{

        public class FeedHolder extends RecyclerView.ViewHolder{

            private TextView feedDate;
            private TextView feedSummary;
            private TextView feedDescription;
            private FlickFeedModel model;

            public FeedHolder(@NonNull View itemView, FlickFeedModel model) {
                super(itemView);
                feedDate = itemView.findViewById(R.id.feedDate);
                feedSummary = itemView.findViewById(R.id.feedSummary);
                feedDescription = itemView.findViewById(R.id.feedDescription);
                this.model = model;
            }

            public void bind(){
                feedDate.setText(model.getFeedDateAsString());
                feedDescription.setText(model.feedDescription);
            }
        }

        private List<FlickFeedModel> feedModels;

        public FlickrFeedArrayAdtp(List<FlickFeedModel> feedModels) {
            this.feedModels = feedModels;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public FeedHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(FeedActivity.this).inflate(R.layout.flickr_single_feed, viewGroup, false);
            return new FeedHolder(view, feedModels.get(i));
        }

        @Override
        public void onBindViewHolder(@NonNull FlickrFeedArrayAdtp.FeedHolder feedHolder, int i) {
            feedHolder.bind();
        }


        @Override
        public int getItemCount() {
            return feedModels.size();
        }
    }
}
