package zver.ru.geekbrainsapp.view;

import org.reactivestreams.Subscriber;

public interface BaseNetView extends Subscriber {

    void onDownLoad();

    void onDownloadComplete();

    void showEmpty();
}
