package zver.ru.geekbrainsapp;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import zver.ru.geekbrainsapp.model.FlickrRoomDataBbase;

public class OrmApp extends Application {
    private static final String DATABASE_NAME = "ROOM_FEEDS_DB";
    public static FlickrRoomDataBbase database;
    public static OrmApp INSTANCE;
    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(getApplicationContext(), FlickrRoomDataBbase.class, DATABASE_NAME).build();
        INSTANCE = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
    }
