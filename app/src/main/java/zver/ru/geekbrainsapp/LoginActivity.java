package zver.ru.geekbrainsapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import zver.ru.geekbrainsapp.presentor.BasePresentor;
import zver.ru.geekbrainsapp.view.BaseNetView;

import static java.lang.Thread.sleep;

public class LoginActivity extends Activity implements BaseNetView {

    private Button btn_login;
    private EditText ed_login, ed_password;

    private List<View> activeList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        btn_login = findViewById(R.id.btn_checkPassword);
        ed_login = findViewById(R.id.ed_login);
        ed_password = findViewById(R.id.ed_password);

        activeList.add(btn_login);
        activeList.add(ed_login);
        activeList.add(ed_password);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_login.setActivated(false);
                presentor.startProcess();
            }
        });
    }

    private final BasePresentor presentor = new BasePresentor(this) {
        @Override
        public void startProcess() {
            Toast.makeText(LoginActivity.this, "Йа кнопко!", Toast.LENGTH_SHORT).show();
            try {
                onDownLoad();
                sleep(1000);
                onDownloadComplete();
            } catch (InterruptedException e) {
                e.printStackTrace();
                onError(e);
            }
        }
    };

    @Override
    public void onDownLoad() {
        setActivated(false);
    }

    @Override
    public void onDownloadComplete() {
        setActivated(true);
        Toast.makeText(this, "Загрузка завершена", Toast.LENGTH_SHORT).show();

    }

    private void setActivated(boolean activated){
        for(View view: activeList){
            view.setActivated(activated);
        }
    }

    @Override
    public void showEmpty() {

    }

    @Override
    public void onSubscribe(Subscription s) {

    }

    @Override
    public void onNext(Object o) {

    }

    @Override
    public void onError(Throwable t) {
        setActivated(true);
        Toast.makeText(this, "Произошли ошибки!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete() {

    }
}
