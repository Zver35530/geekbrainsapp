package zver.ru.geekbrainsapp.presentor;


import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import zver.ru.geekbrainsapp.OrmApp;
import zver.ru.geekbrainsapp.model.AbstractModel;
import zver.ru.geekbrainsapp.model.FlickFeedModel;
import zver.ru.geekbrainsapp.view.FeedsNetView;

public class FlickrFeedPresentor extends BasePresentor<AbstractModel> {

    private final FeedsNetView feedsNetView;

    private List<FlickFeedModel> flickFeedModelList = new ArrayList<>();

    public FlickrFeedPresentor(FeedsNetView view) {
        super(view);

        feedsNetView = view;
    }

    @Override
    protected void subscribeActual(Subscriber s) {
        super.subscribeActual(s);

        for(int iCnt = 0; iCnt < 1_000; iCnt++){
            s.onNext(new FlickFeedModel("New photo", new Date(System.currentTimeMillis() - 1_000 * iCnt), "Новая фотка"));
        }
        this.subscribeOn(Schedulers.io());
        this.observeOn(Schedulers.io()).concatWith(new Publisher() {
            @Override
            public void subscribe(Subscriber s) {
                OrmApp.database.getDao().getAll();
            }
        });

        long startTime = System.currentTimeMillis();
        for(int iCnt = 0; iCnt < 1_000; iCnt++){
           // this.observeOn(Schedulers.computation()). OrmApp.database.getDao().insertSingle( new FlickFeedModel("New photo", new Date(System.currentTimeMillis() - 1_000 * iCnt), "Новая фотка"));
        }

        long durance = System.currentTimeMillis() - startTime;

        //flickFeedModelList = OrmApp.database.getDao().getAll();

        //feedsNetView.setFeeds(flickFeedModelList);

        s.onComplete();
    }

    @Override
    public void startProcess() {

        feedsNetView.onDownLoad();

       /* List<FlickFeedModel> feedModelList = new ArrayList<>();

        FlickFeedModel model;
        long startTime = System.currentTimeMillis();
        for(int iCnt = 0; iCnt < 1_000; iCnt++){
            model = new FlickFeedModel("New photo", new Date(System.currentTimeMillis() - 1_000 * iCnt), "Новая фотка");
            feedModelList.add(model);
        }

        OrmApp.database.getDao().insertList(feedModelList);

        long durance = System.currentTimeMillis() - startTime;

        feedsNetView.setFeeds(OrmApp.database.getDao().getAll());*/
        feedsNetView.onDownloadComplete();
    }

}
