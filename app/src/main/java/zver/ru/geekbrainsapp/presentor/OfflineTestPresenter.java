package zver.ru.geekbrainsapp.presentor;

import zver.ru.geekbrainsapp.model.AbstractModel;
import zver.ru.geekbrainsapp.view.BaseNetView;

public class OfflineTestPresenter extends BasePresentor<AbstractModel> {

    public OfflineTestPresenter(BaseNetView appBaseView){
        super(appBaseView);
    }

    @Override
    public void startProcess() {
        view.onDownLoad();

        try {
            wait(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
