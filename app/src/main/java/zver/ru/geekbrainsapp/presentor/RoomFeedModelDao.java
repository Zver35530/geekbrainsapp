package zver.ru.geekbrainsapp.presentor;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import zver.ru.geekbrainsapp.model.FlickFeedModel;

@Dao
public interface RoomFeedModelDao {
    @Query("Select * from flickfeedmodel order by feedDate desc")
    abstract List<FlickFeedModel> getAll();

    @Insert
    void insertList(List<FlickFeedModel> modelList);

    /**Инетересно попробовать сравнить что быстрее
     * */
    @Insert
    void insertSingle(FlickFeedModel model);

    @Delete
    void delete(FlickFeedModel model);

    @Query("Delete from flickfeedmodel")
    void clear();
}
