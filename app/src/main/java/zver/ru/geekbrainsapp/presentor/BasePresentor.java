package zver.ru.geekbrainsapp.presentor;

import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import zver.ru.geekbrainsapp.model.AbstractModel;
import zver.ru.geekbrainsapp.view.BaseNetView;

public abstract class BasePresentor < M extends AbstractModel> extends Flowable implements Subscriber {

    List<Subscriber> subscriberList = new ArrayList<>();

    protected final BaseNetView view;

    public BasePresentor(BaseNetView view) {
        this.view = view;
    }

    abstract public void startProcess();

    @Override
    protected void subscribeActual(Subscriber s) {
        subscriberList.add(s);
    }
}
